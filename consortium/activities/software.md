---
title: Software
layout: page
permalink: /consortium/activities/software/
---
The Consortium funds software development activities that benefit the open source project.  

In 2021-22, we are funding **WiGig module maintenance** via maintainer Sebastien Deronne.
  * **initial phase** (through Sept. 2021): **[Progress report](https://www.nsnam.org/wp-content/uploads/2021/wigig-report-sept-2021.pdf)**
  * **current phase** (initiated Oct. 2021): Port WiGig classes to new architecture, add DMG PHY entity, add DMG MAC FEM.

In 2020, we funded **ns-3 wifi module maintenance** via maintainer Sebastien Deronne.  The work was conducted in two phases, as summarized in the following reports:

  * **Phase 1** (through May 2020): **[PHY abstraction, Bianchi example, and rate controls](https://www.nsnam.org/wp-content/uploads/2020/ns-3-wifi-module-upgrade-phase-1.pdf)**  
  * **Phase 2** (through December 2020): **[LAA-Wifi coexistence, PHY error models, and PHY refactoring work](https://www.nsnam.org/wp-content/uploads/2021/ns-3-wifi-module-upgrade-phase-2.pdf)** 

We have also funded **[website development and content migration](https://finance.uw.edu/c2/design-web/getting-started)** for [the main web site](https://www.nsnam.org).

The previously organized ns-3 Consortium (which ran from 2012-2018) funded the following.

  * The port of [ns-3 for Visual Studio 2012](http://www.nsnam.org/wiki/Ns-3_on_Visual_Studio_2012) was organized by the Consortium in 2013.
