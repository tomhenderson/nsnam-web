---
layout: post
title:  "WNS3 dates and venue announced"
date:   2019-11-22 09:30:00 -0700
categories: events
---
The project will be holding its annual meeting during the week of June 15, 2020, in Gaithersburg, MD, hosted by [NIST](https://www.nist.gov/).  The Workshop on ns-3 [Call for Papers](/research/wns3/wns3-2020/call-for-papers/) is posted; the paper submission deadline is February 16, 2020.
