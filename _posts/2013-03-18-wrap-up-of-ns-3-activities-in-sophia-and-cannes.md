---
id: 2225
title: Wrap-up of ns-3 activities in Sophia and Cannes
date: 2013-03-18T05:47:41+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=2225
permalink: /news/wrap-up-of-ns-3-activities-in-sophia-and-cannes/
categories:
  - Events
  - News
---
The project had several days of activities in the south of France, March 4-8, 2013. The first day was devoted to the first annual meeting of the ns-3 Consortium. Materials for these talks and tutorials are available [here](http://www.nsnam.org/consortium/activities/annual-meeting-march-2013/). The 5th annual Workshop on ns-3 was held in Cannes, France on 5 March, at the SIMUTools conference site. Slides from [these talks](http://www.nsnam.org/wns3/wns3-2013/) will be posted shortly. Finally, a productive three-day developers meeting was held at Inria, and meeting notes are available [here](http://www.nsnam.org/docs/meetings/ns-3-developer-meeting-notes-Mar13.pdf).