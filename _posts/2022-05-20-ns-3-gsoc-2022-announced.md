---
title: ns-3 GSoC selections announced
date: 2022-05-20T11:00:00+00:00
author: tomh
layout: post
permalink: /news/ns-3-gsoc-contributors-announced-2022/
categories:
  - News
---
Three students will work with the ns-3 project for the [2022 Google Summer of Code](https://www.nsnam.org/wiki/Summer_Projects#Google_Summer_of_Code_2022)! 

The selected students, projects, and project mentors are:

* **Akash Mondal**, [TCP maximum segment size (MSS) improvements](https://summerofcode.withgoogle.com/programs/2022/projects/QsOiooM7), mentored by Mohit Tahiliani, Bhaskar Kataria, and Vivek Jain
* **Matteo Pagin**, [A simplified channel and beamforming model for ns-3](https://summerofcode.withgoogle.com/programs/2022/projects/uxXqy83g), mentored by Sandra Lagen and Biljana Bojovic
* **Zhiheng Dong**, [Perfect ARP and NDP](https://summerofcode.withgoogle.com/programs/2022/projects/DdUT09pZ), mentored by Tommaso Pecorella, Ameya Deshpande,and Manoj Kumar Rana
