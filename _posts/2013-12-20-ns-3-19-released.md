---
id: 2623
title: ns-3.19 released
date: 2013-12-20T22:00:03+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=2623
permalink: /news/ns-3-19-released/
categories:
  - News
  - ns-3 Releases
---
[ns-3.19](https://www.nsnam.org/release/ns-allinone-3.19.tar.bz2) was released on 20 December 2013. This release features LTE model extensions to UE measurements and improved handover algorithm models, and a new WiFi extension for vehicular simulation support to realize an IEEE 802.11p-compliant device. A new IPv6 over Low power Wireless Personal Area Networks (6LoWPAN) model is available, intended in the future to enable IPv6 over sensor network models. A new FixedRoomPositionAllocator has been added to the buildings module. It allows one to generate a random position uniformly distributed in the volume of a chosen room inside a chosen building. Also regarding LTE, the code of LteMiErrorModel has been optimized, resulting in a significantly lower execution time of the LTE model when used with the error model enabled. A new parallel scheduling algorithm based on null messages, a common parallel DES scheduling algorithm, has been added. The null message scheduler has better scaling properties when running on some scenarios with large numbers of nodes since it does not require a global communication. Finally, some patches were added to facilitate easier use of ns-3 with the mininet emulator.