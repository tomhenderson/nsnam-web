---
layout: post
title:  "WNS3 deadline extended to 02/25"
date:   2022-02-20 00:23:00 -0800
categories: events
---

The deadline for paper submissions to the Workshop on ns-3 (WNS3) 2022 has been extended to Friday February 25th (firm), due to the change to a fully virtual event.

Information regarding the workshop, including the link for the submission
site and acceptance criteria, is available at
<https://www.nsnam.org/research/wns3/wns3-2022/call-for-papers/>

Considering the current situation, the organizing committee has decided to
switch to a virtual workshop for June 22-23. Following the format of WNS3
2020 and 2021, additional events will be planned during the same week,
including a developer meeting and the annual ns-3 Consortium meeting, and
possibly training sessions. More information on this will follow.

Looking forward to receiving your submissions!
