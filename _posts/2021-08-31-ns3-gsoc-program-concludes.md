---
title: ns-3 GSoC program concludes
date: 2021-08-31T18:00:00+00:00
layout: post
permalink: /news/2021-ns-3-gsoc-program-concludes/
categories:
  - News
---
Three students successfully completed Google Summer of Code with the ns-3 project:
 * Parth Pratim Chatterjee, Direct Code Execution Modernization
 * Ameya Deshpande, IPv6 Nix-Vector Routing
 * Akshit Patel, Add logging support to Simulation Execution Manager (SEM)
<!--more-->

Thanks to both the students and mentors for their efforts.  Detailed project reports for each project are linked <a href="https://www.nsnam.org/wiki/Summer_Projects#Google_Summer_of_Code_2021">here</a>.
