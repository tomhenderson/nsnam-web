---
id: 2534
title: ns-3 at ACM MSWiM
date: 2013-11-03T22:42:36+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=2534
permalink: /news/ns-3-at-acm-mswim/
categories:
  - Events
  - News
---
Researchers from CTTC, the University of Tokyo, and INRIA will be presenting new ns-3 capabilities at the upcoming [16th ACM International Conference on Modeling, Analysis and Simulation of Wireless and Mobile Systems (MSWiM)](http://mswimconf.com/2013/) in Barcelona on November 3-8.

In the main technical track, CTTC researchers will present their work titled &#8220;An open source model for the simulation of LTE handover scenarios and algorithms in ns-3&#8221;. This work describes the new LTE functionalities for the simulation of handover scenarios and algorithms developed within [the LENA project](http://iptechwiki.cttc.es/LTE-EPC_Network_Simulator_%28LENA%29) and merged in ns-3.18, which was released in August 2013. The presentation will provide an overview of the additional features added to the previous ns-3 version, such as the modeling of the RRC protocol, of the MAC random access procedure, and of the X2, S1 and S11 interfaces, and will explain how the LTE handover procedure is modeled on top of these features, highlighting the modeling assumptions that were made.

In the demonstration track, a team from University of Tokyo and INRIA will present a demonstration on &#8220;Direct Code Execution: Increase Simulation Realism using Unmodified Real Implementations&#8221;. The demonstration will show attendees [Direct Code Execution (DCE)](http://www.nsnam.org/about/projects/direct-code-execution/), running POSIX socket based application and Linux kernel network stacks over ns-3 network simulator, which gives us a functional realism of simulated protocols. Two typical use cases of DCE will be presented: information-centric networking using the PARC CCNx code, and a seamless handoff experiment based on a Linux Multipath TCP implementation.