---
title: ns-3 GSoC program concludes
date: 2020-09-07T18:00:00+00:00
layout: post
permalink: /news/2020-ns-3-gsoc-program-concludes/
categories:
  - News
---
Four students successfully completed Google Summer of Code with the ns-3 project:
 * Shivamani Patil, App Store Improvements
 * Ananthakrishnan S, NetDevice up/down consistency and event chain
 * Bhaskar Kataria, SCE AQMs and TCP along with CNQ-CodelAF and LFQ
 * Deepak K, TCP Prague model for ns-3
<!--more-->

Thanks to both the students and mentors for their efforts.  Detailed project reports for each project are linked <a href="https://www.nsnam.org/wiki/Summer_Projects#Google_Summer_of_Code_2020">here</a>.
