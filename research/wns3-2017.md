---
layout: page
title: WNS3 2017
permalink: /research/wns3/wns3-2017/
---
Many thanks to [INESC TEC](https://www.inesctec.pt/ip-en) and [Universidade do Porto &#8211; Faculdade de Engenharia](https://sigarra.up.pt/feup/en/web_page.inicial) for hosting the ns-3 annual meeting including the Workshop on ns-3 (WNS3) on June 13-14, 2017 in Porto, Portugal. The objective of the annual meeting and workshop is to gather ns-3 users and developers, together with networking simulation practitioners and users, and developers of other network simulation tools, to discuss the ns-3 simulator and related activities.

WNS3 is part of the ns-3 annual meeting, organized by the [ns-3 Consortium](https://www.nsnam.org/consortium/).
