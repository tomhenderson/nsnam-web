---
layout: page
title: Travel grants
permalink: /research/wns3/wns3-2024/travel-grants/
---

The Workshop on ns-3 (WNS3) is pleased to announce the availability of 4 travel grants
sponsored by [TELECO RENTA](https://telecorenta.es/), the Spanish plan for the promotion of telecommunications
studies in Spain (PPET), funded by the Spanish Ministry of Economic Affairs and Digital
Transformation through PRTR and the European Union- NextGenerationEU.
Applications are invited from qualified full-time graduate and undergraduate students. To qualify,
a student must:

* be a full-time student registered towards a Master or Ph.D. degree in engineering or related field in a college or university when submitting the application;
* having never been enrolled in a Spanish university, and
* having not received a similar grant in the past 12 months.

Women, undergraduates, and underrepresented minority students are particularly encouraged
to apply.
The travel grant will cover the conference registration, travel expenses and accommodation up
to a maximum of:
* 2.500€ for overseas students (outside Europe)
* 1.500€ for students within Europe.

For more information, please consult [this file](/workshops/wns3-2024/Renta-Travel-Grant-WNS3-2024-CTTC.pdf).  Applications are due by May 6 (extended from April 29).

<a><img src="/wp-content/uploads/2024/sponsorship_telecorenta.png" alt="Teleco RENTA"/></a>
