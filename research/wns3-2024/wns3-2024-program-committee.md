---
layout: page
title: Program Committee
permalink: /research/wns3/wns3-2024/program-committee/
---

# Technical Program Co-Chairs

* Pasquale Imputato, University of Naples Federico II <p.imputato@gmail.com>
* Mohit Tahiliani, NITK Surathkal <tahiliani.nitk@gmail.com>

# Proceedings Chair

* Eric Gamess, Jacksonville State University <egamess@gmail.com>

# General Chair

* Sandra Lagen, CTTC <slagen@cttc.es>

# Technical Program Committee

Name                 | Institution                                 | 
---------------------|-----------------------------------------------
Ramon Aguero         | University of Cantabria, Spain |
Hany Assasa          | Pharrowtech, Belgium |
Stefano Avallone     | University of Naples “Federico II”, Italy |
Peter Barnes         | Lawrence Livermore National Laboratory, USA |
Douglas Blough       | Georgia Institute of Technology, USA |
Biljana Bojovic      | Centre Tecnològic de Telecomunicacions de Catalunya, Spain |
Federico Chiariotti  | Aalborg University, Denmark |
Helder Fontes        | INESC and FEUP |
Eric Gamess          | Jacksonville State University, USA |
Felipe Gomez-Cuba    | University of Vigo, Spain |
Tom Henderson        | University of Washington, USA |
Pasquale Imputato    | University of Naples “Federico II”, Italy |
Anil Jangam          | Cisco Systems, USA |
Luciano Jerez Chaves | UNICAMP, Brazil |
Sian Jin             | Mathworks, USA |
Katerina Koutlia     | Centre Tecnològic de Telecomunicacions de Catalunya, Spain |
Yuchen Liu           | North Carolina State University, USA |
Sharan Naribole      | Apple Inc., USA |
Michele Polese       | Northeastern University, USA |
Mohit Tahiliani      | National Institute of Technology, Karnataka, Surathkal
Hajime Tazaki        | IIJ Innovation Institute, Japan |
Thierry Turletti     | Inria, France |
Tommaso Zugno        | Huawei Technologies |

