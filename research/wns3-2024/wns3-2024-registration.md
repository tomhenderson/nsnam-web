---
layout: page
title: Registration 
permalink: /research/wns3/wns3-2024/registration/
---

Registration is being handled through the University of Washington Housing and Food Services [registration site](https://washington.irisregistration.com/Form/6320).  Registration can be paid with a credit card.

Registration fees cover the cost of providing coffee and break food and other incidentals.  Attendees will be responsible for purchasing their own lunch daily (the CTTC canteen will be available), and for having other meals outside of the conference.  We will have a social dinner event at a restaurant in Barcelona on the night of Wednesday June 5.  Tickets for the dinner can be purchased for 25 Euros on-site at CTTC.

Registration will be offered day-by-day; attendees can pay for the days that they wish to attend.  The fee structure is as follows.  Please be aware that after May 20, a $15 late registration fee will be applied.

| Date        | Agenda           |  Cost  |
| ----------- | ---------------- | ------ |
| Mon. June 3 | Basic training   |  $30   |
| ----------- | ---------------- | ------ |
| Tue. June 4 | Talks and adv. tutorials   |  $30   |
| ----------- | ---------------- | ------ |
| Wed. June 5 | Workshop on ns-3 |  $30   |
| ----------- | ---------------- | ------ |
| Thu. June 6 | Developer mtgs.  | free   |
| ----------- | ---------------- | ------ |

Additionally, the speaker for each workshop paper is requested to register for an additional $100 fee to support our publication fees for the ACM digital library.
