---
layout: page
title: WNS3 2024 Local Information
permalink: /research/wns3/wns3-2024/local-information/
---

# General Chair

Contact [Sandra Lagen](mailto:slagen@cttc.es) for any specific questions about local information.

# Location of the meeting

The meeting will be held at the [Centre Tecnològic de Telecomunicacions de Catalunya (CTTC)](https://www.cttc.cat/).

# Local hotels

CTTC has provided this [information sheet](https://www.nsnam.org/workshops/wns3-2024/wns3-2024-hotel-list.pdf) about local hotel options.
