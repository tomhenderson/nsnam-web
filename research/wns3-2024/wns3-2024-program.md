---
layout: page
title: WNS3 2024 Program
permalink: /research/wns3/wns3-2024/program/
---

The **Workshop on ns-3** will be a single day workshop on Wednesday June 5, included as part of the **ns-3 annual meeting** which runs from June 3 through June 6.  The following **tentative** program will be updated once all talks and tutorials are finalized.

The annual meeting will largely be an in-person event, with some sessions
recorded for later archiving on Vimeo.  The lightning talks session on
Wednesday will be a hybrid event via a free Zoom meeting.

# Monday, June 3:  ns-3 training

ns-3 training is an introduction to ns-3, for users just starting with ns-3. We will provide a comprehensive overview of the most commonly used parts of ns-3.  This session has not been offered since 2019; videos and slides of 
[past sessions](https://www.nsnam.org/consortium/activities/training/) from 2019 and earlier are archived.

# Tuesday, June 4:  Invited talks and advanced tutorials

This day will consist of invited talks and advanced tutorials.

09:00-10:30: NR module: A general overview and new NR 3.0 MIMO models and usage (Biljana Bojovic) 

11:00-12:30: 5G NR Sidelink and ProSe models (Tom Henderson)

14:00-15:30: Wi-Fi models and new features

16:00-16:30: status update on ns-3 TCP models (Tom Henderson)

16:30-17:30: ns-3 Consortium annual meeting

The annual business meeting of the [ns-3 Consortium](https://www.nsnam.org/consortium/) will be held at the end of the day.

# Wednesday, June 5:  WNS3 paper presentations and lightning talks

The schedule of WNS3 paper presentations should be published by late April.

09:00-10:00: Introductions and keynote talk
10:00-10:30:  Paper presentation

11:00-12:30:  Three paper presentations

14:00-15:30:  Three paper presentations

16:00-17:30:  Lightning talks

* Gabriel Ferreira, "NR and LTE Performance Improvements"
* Gabriel Ferreira, "Educational use of ns-3".
* Evan Black, "NetSimulyzer update"
* Andrea Lacava, Matteo Pagin,  Tommaso Pietrosanti and Michele Polese, "Toward large-scale online reinforcement learning with sem and ns-3"
* (others TBD)

Lightning talks are brief talks (5-10 minutes) to update the ns-3 community on works-in-progress, updates to previously released models, or new ideas.  This session will be hybrid with remote participation.

Evening:  A social dinner at a local restaurant will be organized for the evening of Wednesday June 5.  More information will be provided closer to the date of the conference, and this event will be handled outside of the University of Washington conference registration process.

# Thursday, June 6:  Maintainer and software development discussions

Thursday will be an informal day for software development discussions among maintainers and any interested attendees.  There is no formal registration for attending this day, and attendance will be open to anyone interested.

