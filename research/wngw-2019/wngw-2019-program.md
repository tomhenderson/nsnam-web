---
layout: page
title: WNGW 2019 Program
permalink: /research/wngw/wngw-2019/program/
---
# Friday, June 21, 2019
*Department of Information Engineering, University of Florence, Italy*

*Lunch and coffee breaks will be provided*

**08:30 am:  Registration**

**09:00 to 10:30 am:  Invited talks**

- Peter Barnes (LLNL), "S3: the Spectrum Sharing Simulator" [(video)](https://vimeo.com/361087033)
- Lorenza Giupponi (CTTC), "5G NR support in ns-3" [(video)](https://vimeo.com/361194552)
- Colin McGuire (MathWorks), "Modelling Next-Generation Wireless Systems" [(video)](https://vimeo.com/361330283)

**10:30 to 11:00 am:  Coffee break**

**11:00 am to 12:40 pm:  Next-generation cellular**

- Tommaso Zugno, Michele Polese, Mattia Lecci and Michele Zorzi (UNIPD), "Simulation of Next-Generation Cellular Networks with ns-3: Open Challenges and New Directions" [(video)](https://vimeo.com/363010676)
- Tommaso Pecorella, Francesca Nizzi, Mattia Bastianini, Carlo Cerboni, Alessandra Buzzigoli and Andrea Fratini (UNIFI), "The role of network simulator in the 5G experimentation" [(video)](https://vimeo.com/363009880)
- Qiang Hu, Yuchen Liu, Yan Yan and Douglas Blough (Ga. Tech), "End-to-end Simulation of MmWave Out-of-band Backhaul Networks in ns-3" [(video)](https://vimeo.com/361965281)
- R. Rouil (NIST), “Future public safety networks in ns-3”
- Clemens Felber (National Instruments), “Prototyping LTE-WiFi Interworking on a Single SDR Platform” [(video)](https://vimeo.com/365102329)

**12:40 to 13:45 pm:  Lunch break**

**13:45 to 15:25 pm:  IEEE 802.11 wideband technologies**

- Stefano Avallone (UNINA) and Sebastien Deronne (Televic Conference), "Emerging ns-3 support for 802.11ax" [(video)](https://vimeo.com/365272420),[(slides)](https://www.nsnam.org/wp-content/uploads/2019/WNGW-11ax-presentation.pptx)
- Weihua Jiang (Xiamen Univ.), Sumit Roy (UW) and Colin McGuire (MathWorks), "Efficient Link-to-System Mappings for MU-MIMO Channel D Scenarios in 802.11ac WLANs" [(video)](https://vimeo.com/364304833)
- Francesc Wilhelmi, Sergio Barrachina-Muñoz (Universitat Pompeu Fabra (UPF)), "Towards the implementation of 11ax features in Komondor" [(video)](https://vimeo.com/365304460)
- Hany Assasa, Joerg Widmer (IMDEA), Tanguy Ropitault, Anuraag Bodi and Nada Golmie (NIST), "High Fidelity Simulation of IEEE 802.11ad in ns-3 Using a Quasi-deterministic Channel Model" [(video)](https://vimeo.com/367572555)
- Hany Assasa, Joerg Widmer (IMDEA), Jian Wang, Tanguy Ropitault and Nada Golmie(NIST), "An Implementation Proposal for IEEE 802.11ay SU/MU-MIMO Communication in ns-3" [(video)](https://vimeo.com/367577259)

**15:30 to 16:00 pm:  Panel discussion on furthering ns-3 and 802.11ax**
- Moderator:  Leonardo Lanante (Kyushu Institute of Technology)
- Panelists:  Getachew Redietab (Orange), M. Shahwaiz Afaqui (UOC)
- Topic:  How to facilitate larger scale involvement with and testing of ns-3 802.11ax models.

**16:00 to 16:30 pm:  Coffee break**

**16:30 to 17:30 pm:  Narrowband wireless**

- Ashish Kumar Sultania, Carmen Delgado and Jeroen Famaey (Univ. Antwerp), "Implementation of NB-IoT Power Saving Schemes in NS-3" [(video)](https://vimeo.com/368162162)
- Serena Santi, Le Tian and Jeroen Famaey (Univ. Antwerp), "Evaluation of the Co-Existence of RAW and TWT Stations in IEEE 802.11ah using NS-3" [(video)](https://vimeo.com/368189730)
- Tommaso Pecorella and Francesca Nizzi (UNIFI), "Protocol prototype implementation using ns-3 -- a use-case" [(video)](https://vimeo.com/368624150)

**17:30 to 18:00 pm:  Closing remarks (T. Henderson and S. Roy)**

**18:00 pm:  Adjourn**
