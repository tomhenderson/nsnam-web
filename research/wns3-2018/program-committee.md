---
title: Program Committee
layout: page
permalink: /research/wns3/wns3-2018/program-committee/
---
### **General Chair**

  * Mohit Tahiliani, NITK Surathkal, <tahiliani.nitk@gmail.com>

### **Technical Program Committee Co-Chairs**

  * Eric Gamess, Jacksonville State University <egamess@gmail.com>
  * Damien Saucez, INRIA <damien.saucez@inria.fr>

### **Proceedings Chair**

  * Eric Gamess, Jacksonville State University <egamess@gmail.com>

### **Technical Program Committee Members**

The current list of the technical program committee is:

  * Alexander Afanasyev, University of California at Los Angeles, USA
  * Ramón Agüero, University of Cantabria, Spain
  * Peter Barnes, Lawrence Livermore National Laboratory, USA
  * Matthieu Coudron, IIJ Innovation Institute, Japan
  * Sébastien Deronne, Televic Conference, Belgium
  * Guillermo Francia, Jacksonville State University, USA
  * Tom Henderson, University of Washington, USA
  * Anil Jangam, Cisco Systems, USA
  * Sam Jansen, Starleaf Ltd, United Kingdom
  * Kevin Jin, Illinois Institute of Technology, USA
  * Spyridon Mastorakis, University of California at Los Angeles, USA
  * Ruben Merz, Swisscom, Switzerland
  * Carlos Moreno, Central University of Venezuela, Venezuela
  * Tommaso Pecorella, Università di Firenze, Italia
  * Gary Pei, The Boeing Company, USA
  * Manuel Ricardo, INESC TEC, Portugal
  * Samar Shailendra, TCS Research & Innovation, India
  * Giovanni Stea, University of Pisa, Italia
  * Mohit Tahiliani, National Institute of Technology Karnataka, India
  * Hajime Tazaki, IIJ Innovation Institute, Japan
  * Thierry Turletti, Inria, France
  * Mythili Vutukuru, Indian Institute of Technology Bombay, India
  * Joerg Widmer, IMDEA Networks, Spain

### **Steering Committee**

The <a href="https://www.nsnam.org/consortium/governance/" target="_blank">ns-3 Consortium steering committee</a> serves also as the WNS3 steering committee.
