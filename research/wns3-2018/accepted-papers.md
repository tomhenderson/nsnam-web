---
title: Accepted Papers
layout: page
permalink: /research/wns3/wns3-2018/accepted-papers/
---
Proceedings have been published in the <a href="https://dl.acm.org/citation.cfm?id=3199902" target="_blank">ACM Digital Library</a>.

  * **Mark Claypool, Jae Chung, and Feng Li.**&nbsp; BBR' &#8211; An Implementation of Bottleneck Bandwidth and Round-trip Time Congestion Control for ns-3
  * **Amina Sljivo, Dwight Kerkhove, Ingrid Moerman, Eli De Poorter, and Jeroen Hoebeke.** InteractiveWeb Visualizer for IEEE 802.11ah ns-3 Module
  * **Biljana Bojovic, Sandra Lagen, and Lorenza Giupponi.**&nbsp; Implementation and Evaluation of Frequency Division Multiplexing of Numerologies for 5G New Radio in ns-3
  * _(Awarded Best Paper)_&nbsp;**Le Tian, Amina &Scaron;ljivo, Serena Santi, Eli De Poorter, Jeroen Hoebeke, and Jeroen Famaey.&nbsp;** Extension of the IEEE 802.11ah ns-3 Simulation Module
  * **Justin Rohrer and Andrew Mauldin.** Implementation of Epidemic Routing with IP Convergence Layer in ns-3
  * **Peter Kourzanov.&nbsp;** Live Network Simulation in Julia &#8211; Design and Implementation of LiveSim.jl
  * **Tommaso Zugno, Michele Polese, and Michele Zorzi.&nbsp;** Integration of Carrier Aggregation and Dual Connectivity for the ns-3 mmWave Module
  * **Viyom Mittal, Vivek Jain, and Mohit Tahiliani.&nbsp;** Proportional Rate Reduction for ns-3 TCP
  * **Vivek Jain, Viyom Mittal, and Mohit Tahiliani.&nbsp;** Design and Implementation of TCP BBR in ns-3
  * **Helder Fontes, Rui Campos, and Manuel Ricardo.**&nbsp; Improving the ns-3 TraceBasedPropagationLossModel to Support Multiple Access Wireless Scenarios
  * **Brecht Reynders, Qing Wang, and Sofie Pollin.&nbsp;** A LoRaWAN Module for ns-3: Implementation and Evaluation
