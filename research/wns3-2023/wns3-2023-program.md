---
layout: page
title: WNS3 2023 Program
permalink: /research/wns3/wns3-2023/program/
---

The meeting ran from 9:00am to 5:00pm or 5:30pm, Eastern Daylight Time.

# Monday, June 26:  NR Sidelink and V2X tutorials and discussions

**09:00-10:30 EDT, Tutorial:** Recent Updates to NR Sidelink Sensing, Scheduling, and HARQ Models, *Tom Henderson (University of Washington) and Samantha Gamboa (National Institute of Standards and Technology (NIST)/Prometheus Computing)*; [slides](https://www.nsnam.org/tutorials/consortium23/sidelink-update-tutorial-wns3-2023.pdf); [video](https://vimeo.com/manage/videos/852188697)

**11:00-12:30, Tutorial:** NR C-V2X: ns-3 Operations, Validation, and Experimentation, *Liu Cao and Collin Brady (University of Washington)*; [slides](https://www.nsnam.org/tutorials/consortium23/nr-cv2x-slides-wns3-2023.pdf); [video](https://vimeo.com/manage/videos/852190850)

**13:30-15:00, Tutorial:** Proximity Services (ProSe) Support for 5G NR Simulations, *Aziza Ben Mosbah and Samantha Gamboa (National Institute of Standards and Technology (NIST)/Prometheus Computing)*; [slides](https://www.nsnam.org/tutorials/consortium23/nr-prose-slides-wns3-2023.pdf); [simulation commands](https://www.nsnam.org/tutorials/consortium23/wns3-2023_NR-ProSe-Tutorial_Commands.txt); [video](https://vimeo.com/manage/videos/852193686)

**15:30-17:30, Panel discussion on 5G sidelink:** Panelists will share their experiences or plans with 5G sidelink simulations, testbeds, or other related engineering activities.

Organizers:  Sumit Roy (University of Washington) and Richard Rouil (NIST)

Panelists:

* Tugba Erpek, representing the National Spectrum Consortium [Working Group on Sidelink](https://www.nationalspectrumconsortium.org/working-groups/)
* Vijitha Weerackody (JHU-APL)
* DJ Shyy (MITRE)
* Viji Raveendran (Qualcomm)
* Bo Ryu (EpiSci)

# Tuesday, June 27:  Various meetings, invited talks, and tutorials

**9:00-10:00 EDT, Tutorial:**  Bridging ns-3 and O-RAN: a tutorial on ns-O-RAN, *Andrea Lacava and Michele Polese (Northeastern University)*; [slides](https://www.nsnam.org/tutorials/consortium23/oran-tutorial-slides-wns3-2023.pdf); [video](https://vimeo.com/manage/videos/867704832)

**10:30-12:30, Invited talks:** Invited talks related to AI/ML-based integrations with ns-3

* NetAIGym: Democratizing “Network AI” Research & Development via Simulation-as-a-Service, *Jing Zhu (Intel)*; [slides](https://www.nsnam.org/tutorials/consortium23/netai-gym-slides-wns3-2023.pdf); [video](https://vimeo.com/manage/videos/867709528)
* Integration of Machine Learning with ns-3: Challenges and Opportunities, *Eduardo Nuno Almeida (INESC TEC and Faculdade de Engenharia, Universidade do Porto, Portugal)*; [slides](https://www.nsnam.org/tutorials/consortium23/integration-machine-learning-slides-wns3-2023.pdf); [video](https://vimeo.com/manage/videos/867711549)
* Multi-BSS Wi-Fi Simulations in ns-3, *Sumit Roy (University of Washington)*; [slides](https://www.nsnam.org/tutorials/consortium23/multi-bss-slides-wns3-2023.pdf); [video](https://vimeo.com/manage/videos/867714737)

**13:30-14:00, Invited talk:**  Signal Processing with NS3, *Dr. Colin Funai, Dr. Bishal Thapa (Raytheon BBN)*

**14:00-15:00:** Meeting with US National Science Foundation and industry regarding next steps for ns-3 funding

* NSF: Thyaga Nandagopal, Deep Medhi, Chaitanya Baru
* DoD: Mark Norton (ANSER)
* Federal Labs:  Vijitha Weerakody and Kent Benson (Johns Hopkins APL)
* University participants: Sumit Roy, Tom Henderson (UW), Lingjia Liu (Virginia Tech), Michele Polese (Northeastern), others TBD
* Company participants: Colin Funai (Raytheon BBN), Carlos Cordeiro (Intel), Marcos Martinez Vasquez (MaxLinear)

**15:30-16:00: Invited talk:** Using ns-3 for research on NTN, reinforcement learning-based scheduling, UAV networking, IPC, and dynamic spectrum access, *Dr. Lingjia Liu and students (Virginia Tech)*

**16:00-17:00: Tutorial:** Updates to the ns-3 NetSimulyzer, *Evan Black (NIST)*

# Wednesday, June 28:  WNS3 paper presentations and lightning talks

All papers awarded with the [ACM Artifacts Evaluated](https://www.acm.org/publications/policies/artifact-review-and-badging-current) badge are indicated with the badge ![symbol](/wp-content/uploads/2023/artifacts-available.png).  Four papers were also credited with having exemplary artifacts.

#### Introductions and keynote talk (9:00-10:00 EDT)

**Keynote: Michael Welzl, University of Oslo:** A TCP person’s ns-3 experience report; [video](https://vimeo.com/867716815); [slides](https://www.nsnam.org/workshops/wns3-2023/wns3-keynote-mw.pdf)

**Abstract:** I’m a TCP insider but an ns-3 outsider. A few years ago, I decided to use ns-3 for the first time because it seemed to be the best tool for my purposes - more suitable than ns-2, which I am used to, or real-life tests. This talk will get into both the good and bad sides of my ns-3 experience. First, I will elaborate on difficulties in getting a TCP “Hello World” to work, which, I hope, will shine a constructive light on possible improvements of either the code or the documentation, or both. Then, I will describe what we eventually managed to pull off with ns-3’s emulation capabilities, which I personally found to be an astounding testament of this simulator’s abilities.  This talk will end with a look at the role that ns-3 could play for the future of TCP (and related) research.

#### Paper Session 1 (10:00-10:30):  MIMO

Session chair: *Yuchen Liu*

* ![badge](/wp-content/uploads/2023/artifacts-available.png) **Matteo Pagin, Sandra Lagén, Biljana Bojović, Michele Polese and Michele Zorzi,** Improving the efficiency of MIMO simulations in ns-3; [video](https://vimeo.com/867719757); [slides](https://www.nsnam.org/workshops/wns3-2023/01-pagin-slides-wns3-2023.pdf)

#### Paper Session 2 (11:00-12:30): Channel Modeling

Session chair: *Helder Fontes*

* ![badge](/wp-content/uploads/2023/artifacts-available.png) **Jingyuan Zhang and Douglas M. Blough,** Implementation and Evaluation of IEEE 802.11ax Channel Sounding Frame Exchange in ns-3; [video](https://vimeo.com/867721416); [slides](https://www.nsnam.org/workshops/wns3-2023/02-zhang-slides-wns3-2023.pptx)

* **Hitesh Poddar, Tomoki Yoshimura, Matteo Pagin, Theodore S Rappaport, Art Ishii and Michele Zorzi,** ns-3 Implementation of Sub-Terahertz and MillimeterWave Drop-based NYU Channel Model (NYUSIM); [video](https://vimeo.com/867734708); [slides](/workshops/wns3-2023/03-poddar-slides-wns3-2023.pptx)

* **Mattia Sandri, Matteo Pagin, Marco Giordani and Michele Zorzi,** Implementation of a Channel Model for Non-Terrestrial Networks in ns-3; [video](https://vimeo.com/867736767); [slides](/workshops/wns3-2023/04-sandri-slides-wns3-2023.pdf)

#### Paper Session 3 (13:30-15:00): 5G and THz communications 

Session chair: *Manuel Ricardo*

* ![badge](/wp-content/uploads/2023/artifacts-available.png) **Andrea Lacava, Matteo Bordin, Michele Polese, Rajarajan Sivaraj, Tommaso Zugno, Francesca Cuomo and Tommaso Melodia,** ns-O-RAN: Simulating O-RAN 5G Systems in ns-3; [video](https://vimeo.com/867737402); [slides](/workshops/wns3-2023/05-lacava-slides-wns3-2023.pdf)

* ![badge](/wp-content/uploads/2023/artifacts-available.png) **Katerina Koutlia, Sandra Lagen and Biljana Bojovic,** Enabling QoS Provisioning Support for Delay-Critical Traffic and Multi-Flow Handling in ns-3 5G-LENA; [video](https://vimeo.com/867745416); [slides](/workshops/wns3-2023/06-koutlia-slides-wns3-2023.pdf)

* ![badge](/wp-content/uploads/2023/artifacts-available.png) **Farhan Siddiqui and Bikash Mazumdar** Link Discovery Extension to NS-3’s Terahertz Communication Module; [video](https://vimeo.com/867748869); [slides](/workshops/wns3-2023/07-siddiqui-slides-wns3-2023.pptx)

#### Short Papers and Invited Talk (15:30-17:00)

Session chair: *Michael Welzl*

* **Liu Cao, Lyutianyang Zhang and Sumit Roy,** Efficient PHY Layer Abstraction for 5G NR Sidelink in ns-3; [video](https://vimeo.com/871593131); [slides](/workshops/wns3-2023/sp-01-cao-slides-wns3-2023.pdf)

* ![badge](/wp-content/uploads/2023/artifacts-available.png) *(Exemplary Artifacts)*  **Juan V. Leon Rosas, Thomas R. Henderson and Sumit Roy,** Verification of ns-3 Wi-Fi Rate Adaptation Models on AWGN Channels; [video](https://vimeo.com/871604015); [slides](/workshops/wns3-2023/sp-02-leon-slides-wns3-2023.pptx)

* **Aditya R. Rudra, Sharvani Laxmi Somayaji, Satvik Singh, Saurabh Dhananjay Mokashi, Abhinaba Rakshit, Dayma Khan and Mohit P. Tahiliani,** Linux-like Socket Statistics Utility for ns-3; [video](https://vimeo.com/871610920); [slides](/workshops/wns3-2023/sp-03-rudra-slides-wns3-2023.pptx)

**Invited talk:** From Traces to Transformation: Leveraging ns-3 as a Digital Twin for Next-generation Networks, *Helder Fontes (INESC TEC and Faculdade de Engenharia, Universidade do Porto, Portugal)*; [video](https://vimeo.com/871633032); [slides](/workshops/wns3-2023/fontes-slides-wns3-2023.pdf)

# Thursday, June 29:  WNS3 paper presentations and lightning talks

#### Lightning Talks (09:00-10:30 EDT)

Session chair: *Yuchen Liu*

[session video](https://vimeo.com/899344986)

* **Alberto Gallegos Ramonet,** Extending and Improving Personal Area Networks on ns-3; [slides](/workshops/wns3-2023/thursday/lt-01-gallegos-slides-wns3-2023.pdf)

* **Gabriel Ferreira,** ns-3 Performance and the SPEC CPUv8 Benchmark Suite; [slides](/workshops/wns3-2023/thursday/lt-02-ferreira-slides-wns3-2023.pdf)

* **Gabriel Ferreira,** Pip Packaging and Educational Use of ns-3; [slides](/workshops/wns3-2023/thursday/lt-03-ferreira-slides-wns3-2023.pdf)

* **Jared Ivey,** Towards Efficient Distributed Simulation of Next-Generation Wireless Use Cases in ns-3; [slides](/workshops/wns3-2023/thursday/lt-04-ivey-slides-wns3-2023.pdf)

* **Ang Deng,** A Mobility Model Incorporating Obstacle Avoidance For Evaluation of Proactive Scheduling Algorithms in the mmWave Band; [slides](/workshops/wns3-2023/thursday/lt-05-deng-slides-wns3-2023.pdf)

* **Gongbing Hong,** ns-3 Implementation of ABB CoDel Queue Discipline; [slides](/workshops/wns3-2023/thursday/lt-06-hong-slides-wns3-2023.pdf)

#### Paper Session 4 (11:00-12:30): Machine Learning and Applications

Session chair: *Alberto Gallegos*

* ![badge](/wp-content/uploads/2023/artifacts-available.png) *(Exemplary Artifacts)*  **Wesley Garey, Tanguy Ropitault, Richard Rouil, Evan Black and Weichao Gao,** O-RAN with Machine Learning in ns-3; [slides](/workshops/wns3-2023/thursday/08-garey-slides-wns3-2023.pdf)

* **(Awarded Best Paper)** ![badge](/wp-content/uploads/2023/artifacts-available.png) *(Exemplary Artifacts)*  **Eduardo Nuno Almeida, Helder Fontes, Rui Campos and Manuel Ricardo,** Position-Based Machine Learning Propagation Loss Model Enabling Fast Digital Twins of Wireless Networks in ns-3; [video](https://vimeo.com/899364683); [slides](/workshops/wns3-2023/thursday/09-almeida-slides-wns3-2023.pdf)

* **Peter Detzner, Jana Gödeke, Lars Tönning, Patrick Laskowski, Maximilian Hörstrup, Oliver Stolz, Marius Brehler and Sören Kerner,** SOLA: A Decentralized Communication Middleware Developed with ns-3; [slides](/workshops/wns3-2023/thursday/10-detzner-slides-wns3-2023.pdf)

#### Paper Session 5 (13:30-15:00)  Network and IoT models

Session chair: *Eduardo Almeida*

* ![badge](/wp-content/uploads/2023/artifacts-available.png) *(Exemplary Artifacts)*  **Shengjie Shu, Wenjun Yang, Jianping Pan and Lin Cai,** A Multipath Extension to the QUIC Module for ns-3; [video](https://vimeo.com/872003075); [slides](/workshops/wns3-2023/thursday/11-shu-slides-wns3-2023.pdf)

* **Rui Pedro Monteiro and João Marco Silva,** Flexcomm Simulator: Exploring Energy Flexibility in Software Defined Networks with ns-3; [video](https://vimeo.com/872013984); [slides](/workshops/wns3-2023/thursday/12-monteiro-slides-wns3-2023.pdf)

* ![badge](/wp-content/uploads/2023/artifacts-available.png) **Alberto Gallegos Ramonet, Alexander Guzman Urbina and Kazuhiko Kinoshita,** Evaluation and extension of ns-3 battery framework; [video](https://vimeo.com/872032203); [slides](/workshops/wns3-2023/thursday/13-gallegos-slides-wns3-2023.pdf)

#### Outbrief on WNS3 Artifacts Review (15:30-15:45)

#### Ns-3 Consortium Annual Meeting (15:45-17:00)

The annual business meeting of the [ns-3 Consortium](https://www.nsnam.org/consortium/).

# Friday, June 30:  Maintainer and software development discussions

Friday will be an informal day (mainly for in-person attendees, but a
[Zoom meeting](https://www.nsnam.org/wiki/MaintainersJune2023#Date.2FTime.2FVenue) is provided for
remote attendees).  There is no registration needed
for this day.  A [wiki page](https://www.nsnam.org/wiki/MaintainersJune2023)
will be used to list the agenda and meeting outcomes.

