---
title: Accepted Papers
layout: page
permalink: /research/wns3/wns3-2021/accepted-papers/
---

  * **Hany Assasa, Nina Grosheva, Tanguy Ropitault, Steve Blandino, Nada Golmie and Joerg Widmer,** Implementation and Evaluation of a WLAN IEEE 802.11ay Model in Network Simulator ns-3

  * **Sian Jin, Sumit Roy and Thomas R. Henderson,** EESM-log-AR: An Efficient Error Model for OFDM MIMO Systems over Time-Varying Channels in ns-3

  * **Tommaso Zugno, Matteo Drago, Sandra Lagen, Zoraze Ali and Michele Zorzi,** Extending the ns-3 Spatial Channel Model for Vehicular Scenarios

  * **Yuchen Liu, Shelby Crisp and Douglas Blough,** Performance Study of Statistical and Deterministic Channel Models for mmWave Wi-Fi Networks in ns-3

  * **Katerina Koutlia, Biljana Bojovic, Sandra Lagen and Lorenza Giupponi,** Novel Radio Environment Map for the ns-3 NR Simulator

  * **Vitor Lamela, Helder Fontes, Jose Ruela, Manuel Ricardo and Rui Campos,** Reproducible MIMO Operation in ns-3 using Trace-based Wi-Fi Rate Adaptation

  * **Martina Capuzzo, Carmen Delgado, Jeroen Famaey and Andrea Zanella,** An ns-3 implementation of a battery-less node for energy-harvesting Internet of Things

  * **Evan Black, Samantha Gamboa and Richard Rouil,** NetSimulyzer: a 3D Network Simulation Analyzer for ns-3

  * **Mattia Lecci, Andrea Zanella and Michele Zorzi,** An ns-3 Implementation of a Bursty Traffic Framework for Virtual Reality Sources

  * **Cedrik Schüler, Manuel Patchou, Benjamin Sliwa and Christian Wietfeld,** Robust Machine Learning-Enabled Routing for Highly Mobile Vehicular Networks with PARRoT in ns-3

  * **Davide Magrin, Stefano Avallone, Sumit Roy and Michele Zorzi,** Validation of the ns-3 802.11ax OFDMA Implementation	

  * **Biljana Bojovic, Sandra Lagen and Lorenza Giupponi,** Realistic beamforming design using SRS-based channel estimate for ns-3 5G-LENA module

