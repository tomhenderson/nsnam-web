---
layout: page
title: WNS3 2021
permalink: /research/wns3/wns3-2021/
---

The **Workshop on ns-3 (WNS3)** was held as a fully virtual event, starting
on Monday June 21, 2021 and running for five days.  The objective of the annual workshop is to gather ns-3 users and developers, together with networking simulation practitioners and users, and developers of other network simulation tools, to learn about advances pertaining to the ns-3 simulator.

As in past years, the conference organizers have been accepted by ACM to publish accepted papers in the [ACM ICPS series](https://www.acm.org/publications/icps-series).

The WNS3 is traditionally part of a week-long annual meeting for ns-3, including training and other activities related to ns-3 use and development, including the annual meeting of the [ns-3 Consortium](https://www.nsnam.org/consortium/).  This year we offered a series of advanced tutorials on Monday, Tuesday, and Friday of the meeting week, with the presentation of workshop papers and short talks held on Wednesday and Thursday.

The meeting was held as a Zoom meeting, and registration was free.

* [Registration](/research/wns3/wns3-2021/registration/)
* [Program](/research/wns3/wns3-2021/program/)
* [Tutorials](/research/wns3/wns3-2021/tutorials/)
* [Program Committee](/research/wns3/wns3-2021/program-committee/)
* [Call for Papers](/research/wns3/wns3-2021/call-for-papers/) (already concluded)
* [Call for Tutorials](/research/wns3/wns3-2021/call-for-tutorials/)
* [Call for Lightning Talks](/research/wns3/wns3-2021/call-for-lightning-talks/)
