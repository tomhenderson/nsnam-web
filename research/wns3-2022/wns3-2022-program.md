---
layout: page
title: WNS3 2022 Program
permalink: /research/wns3/wns3-2022/program/
---
# Proceedings

The proceedings are published in the [ACM Digital Library](https://dl.acm.org/doi/proceedings/10.1145/3532577).

# Schedule

The virtual workshop spanned five days (June 20-24) from 9.30am-2pm EDT (13.30-18.00 UTC) each day. 

# Monday, June 20:  Hackathon

**1330-1800 UTC** We will use the meeting slot for a hackathon [(coding sprint)](https://archive.org/details/pyvideo_144___pycon-2009-plenary-intro-to-sprinting).  This event 
is open to anyone (from core maintainer to interested newcomer) who might
be interested in spending some time on improving ns-3 (or learning about
how it is done).

Logistics will be maintained on this [wiki page](https://www.nsnam.org/wiki/Sprints#Future_sprints).

# Tuesday, June 21:  Tutorials

Tutorials are further described (and materials and video links are posted) on the [tutorial page](/research/wns3/wns3-2022/tutorials/).

**1330-1630 UTC, Tutorial 1:** Zoraze Ali (Ericsson, Sweden) and Biljana Bojovic (CTTC, Spain), NR, NR-U and NR-V2X for ns-3: Models and usage

**1700-1800 UTC, Tutorial 2:** Evan Black (National Institute of Standards and Technology (NIST), US), Visualizing a Scenario Using the NetSimulyzer Visualizer

# Wednesday, June 22:  Paper presentations and lightning talks

#### Paper Session 1 (1330 UTC):  Channel Modeling

Session Chair:  Tommaso Zugno

* **Biljana Bojovic, Zoraze Ali, and Sandra Lagen,** ns-3 and 5G-LENA Extensions to Support Dual-Polarized MIMO; [video](https://vimeo.com/manage/videos/740194743); [slides](https://www.nsnam.org/workshops/wns3-2022/01-bojovic-slides.pdf)
* **Andrea Ramos, Yanet Estrada, Miguel Cantero, Jaime Romero, David Martín-Sacristán, Saúl Inca, Manuel Fuentes and José F. Monserrat,** Implementation and Calibration of the 3GPP Industrial Channel Model for ns-3; [video](https://vimeo.com/732255872); [slides](https://www.nsnam.org/workshops/wns3-2022/02-ramos-slides.pptx)
* **Eduardo Nuno Almeida, Mohammed Rushad, Sumanth Reddy Kota, Akshat Nambiar, Hardik L. Harti, Chinmay Gupta, Danish Waseem, Gonçalo Santos, Helder Fontes, Rui Campos and Mohit P. Tahiliani.** Machine Learning based Propagation Loss Module for enabling Digital Twins of Wireless Networks in ns-3; [video](https://vimeo.com/manage/videos/740215170); [slides](https://www.nsnam.org/workshops/wns3-2022/03-almeida-slides.pdf)

#### Paper Session 2 (1430 UTC):  Wi-Fi

Session Chair:  Tom Henderson

* **Hao Yin, Sumit Roy and Sian Jin,** IEEE WLANs in 5 vs 6 GHz: A Comparative Study; [video](https://vimeo.com/732254410); [slides](https://www.nsnam.org/workshops/wns3-2022/04-yin-slides.pdf)
* **Shyam Krishnan Venkateswaran, Ching-Lun Tai, Yoav Ben-Yehezkel, Yaron Alpert and Raghupathy Sivakumar,** IEEE 802.11 PSM in ns-3: A Novel Implementation and Evaluation under Channel Congestion; [video](https://vimeo.com/manage/videos/744056338); [slides](https://www.nsnam.org/workshops/wns3-2022/05-venkateswaran-slides.pptx)
* **Juan Leon, Yacoub Hanna and Kemal Akkaya,** Integration of WAVE and OpenFlow for Realization of SDN-based VANETs in NS-3; [video](https://vimeo.com/732247725); [slides](https://www.nsnam.org/workshops/wns3-2022/06-leon-slides.pptx)

#### Networking Break (1530 UTC)

The zoom session will remain active for discussions and interactions.

#### Paper Session 3 (1600 UTC):  Simulation

Session Chair:  Peter Barnes

* **Jianfei Shao, Minh Vu, Mingrui Zhang, Asmita Jayswal and Lisong Xu,** Symbolic NS-3 for Efficient Exhaustive Testing: Design, Implementation, and Simulations; [video](https://vimeo.com/manage/videos/732254216); [slides](https://www.nsnam.org/workshops/wns3-2022/07-xu-slides.pptx)
* **Parth Pratim Chatterjee and Thomas R. Henderson,** Revitalizing ns-3’s Direct Code Execution; [video](https://vimeo.com/manage/videos/740349921); [slides](https://www.nsnam.org/workshops/wns3-2022/08-chatterjee-slides.pdf)
* **Benjamin Newton, Michael Scoggin, Anand Ganti, Vincent Hietala, and Uzoma Onunkwo,** Clipping for Faster Wireless Network Simulation in ns-3; [video](https://vimeo.com/manage/videos/740362999); [slides](https://www.nsnam.org/workshops/wns3-2022/09-newton-slides.pptx)

#### Lightning talks (1700 UTC):  Session 1

Session Chair:  Yuchen Liu

* **Jared Ivey, Yuchen Liu, Brian Swenson, Doug Blough,** Towards Efficient Distributed Simulation of Next-Generation Wireless Use Cases in ns-3
* **Gabriel Ferreira,** Finding and fixing performance bottlenecks and memory-related bugs in ns-3
* **Torben Petersen,** Bringing the environment to ns-3
* **Dumisa Ngwenya**, Data Collection for TCP Congestion Control Evaluation

# Thursday, June 23:  Paper presentations and lightning talks

#### Paper Session 4 (1330 UTC):  Cellular and IoT

Session Chair:  Sandra Lagen

* **Pascal Jörke, Tim Gebauer and Christian Wietfeld,** From LENA to LENA-NB: Implementation and Performance Evaluation of NB-IoT and Early Data Transmission in ns-3 **(Awarded Best Paper)**; [video](https://vimeo.com/732264543); [slides](https://www.nsnam.org/workshops/wns3-2022/10-joerke-slides.pdf)
* **Muhammad Naeem, Michele Albano, Davide Magrin, Brian Nielsen and Kim Guldstrand Larsen,** A Sigfox Module for the Network Simulator 3; [video](https://vimeo.com/manage/videos/743931343); [slides](https://www.nsnam.org/workshops/wns3-2022/11-naeem-slides.pdf)
* **Argha Sen, Sashank Bonda, Jay Jayatheerthan and Sandip Chakraborty,** Implementation of mmWave-energy Module and Power Saving Schemes in ns-3; [video](https://vimeo.com/732260851); [slides](https://www.nsnam.org/workshops/wns3-2022/12-sen-slides.pdf)

#### Paper Session 5 (1430 UTC):  Machine Learning

Session Chair:  Richard Rouil

* **Emily Ekaireb, Xiaofan Yu, Kazim Ergun, Quanling Zhao, Kai Lee, Muhammad Huzaifa and Tajana Rosing,** ns3-fl: Simulating Federated Learning with ns-3; [video](https://vimeo.com/732263293); [slides](https://www.nsnam.org/workshops/wns3-2022/13-ekaireb-slides.pptx)
* **Peng Zhang, Pedro Forero, Daniel Yap and Dusan Radosevic,** Evaluation of Reinforcement-Learning Queue Management Algorithm for Undersea Acoustic Networks Using NS3; [video](https://vimeo.com/732259116); [slides](https://www.nsnam.org/workshops/wns3-2022/14-zhang-slides.pdf)
* **Matteo Drago, Tommaso Zugno, Federico Mason, Marco Giordani, Mate Boban and Michele Zorzi,** Artificial Intelligence in Vehicular Wireless Networks: A Case Study Using ns-3; [video](https://vimeo.com/manage/videos/743931914); [slides](https://www.nsnam.org/workshops/wns3-2022/15-drago-slides.pdf)

#### Networking Break (1530 UTC)

The zoom session will remain active for discussions and interactions.

#### Paper Session 6 (1600 UTC):  Network and Applications

Session Chair:  Mohit Tahiliani

* **Nicolas Rybowski and Olivier Bonaventure,** Evaluating OSPF convergence with NS3-DCE; [video](https://vimeo.com/732258850); [slides](https://www.nsnam.org/workshops/wns3-2022/16-rybowski-slides.pdf)
* **Biljana Bojovic and Sandra Lagen,** Enabling NGMN mixed traffic models for ns-3; [video](https://vimeo.com/manage/videos/743932361); [slides](https://www.nsnam.org/workshops/wns3-2022/17-bojovic-slides.pdf)


#### Lightning talks (1640 UTC):  Session 2

Session Chair:  Michele Polese

* **Samantha Gamboa,** NR Proximity Services and Sidelink extensions in ns-3
* **Shengjie Shu,** A Multipath Extension to the QUIC Module in ns-3
* **Collin Brady,** Po-Han Huang, Implementation of a Location/CSI Assisted ML Handover Algorithm in ns-3
* **Tanguy Ropitault, Richard Rouil,** Models to evaluate O-RAN architecture in ns-3
* **Andrea Lacava, Tommaso Zugno, Michele Polese,** The O-RAN in the Hat: End to end simulation of O-RAN architecture over ns-3

# Friday, June 24:  Consortium meeting and related events

**1330-1500 UTC, [ns-3 Consortium](https://www.nsnam.org/consortium/) Annual Meeting**

* Consortium summary
  * Hackathon outbrief
* Open source project summary and discussion

<a href="https://www.nsnam.org/wp-content/uploads/2022/ns-3-consortium-talk-2022.final.pdf" target="_blank">annual meeting slides</a>

**note:  postponed to later date** Mohit Tahiliani, Session on usability and ns-3 examples

This session was postponed until September 15, 2022; [video](https://vimeo.com/manage/videos/751902008); [slides](https://www.nsnam.org/wp-content/uploads/2022/ns-3-examples-slides.pdf); [workplan](https://www.nsnam.org/wp-content/uploads/2022/ns-3-examples-plan.pdf)

This session focuses on the usability aspects of ns-3, especially the examples. The ns-3 repository has a large number of examples. Most of the examples are available in the ‘examples/’ directory but quite a few are also available in ‘src/<module-name>/examples’ directories. Typically, a new example is added to the repository when a new feature is introduced in ns-3. However, in the majority of the cases, the new examples being added are not (and sometimes, they cannot be) an incremental update of existing examples. This could be due to the fact that existing examples are not suitable to be updated to demonstrate the new feature being added or maybe the developers find it more convenient to work on the examples that they have already developed for their work. This has led to the ns-3 repository having a large number of examples, but with little to no continuity. Currently, it is not possible to draw a lineage, or a set of small lineages, among the examples that are available in ns-3. Consequently, this makes it difficult for the ns-3 users to incrementally learn the features supported by ns-3. In this session, we will discuss the possible ways to restructure the examples in the ns-3 repository to make it easy for ns-3 users to get started, and comfortably move further with simulating advanced features using ns-3.


