---
title: Presentations
layout: page
permalink: /documentation/presentations/
---
A lot of our users do presentations on how to use ns-3 for various specific purposes but these documents can be more generally useful and are thus included here for wider consumption.

<table border="0">
  <tr>
    <th>
      Title
    </th>
    
    <th>
      Date
    </th>
    
    <th>
      Formats
    </th>
  </tr>
  
  <tr class="even">
    <td>
      Workshop on Simulation of TCP Variants Using ns-3 <a href="/docs/contributed/tcp-variants-workshop-2016/STVN_Workshop-3.pdf"> STVN Workshop brochure</a>
    </td>
    
    <td align="center">
      &nbsp;April 15-16, 2016&nbsp;
    </td>
    
    <td align="center">
      <a href="/docs/contributed/tcp-variants-workshop-2016/1-Session.pdf" target="_blank">Session-1</a>,<br /><a href="/docs/contributed/tcp-variants-workshop-2016/2-Session.pdf" target="_blank">Session-2</a>,<br /><a href="/docs/contributed/tcp-variants-workshop-2016/3-Session.pdf" target="_blank">Session-3</a>,<br /><a href="/docs/contributed/tcp-variants-workshop-2016/4-Session.pdf" target="_blank">Session-4</a>,<br /><a href="/docs/contributed/tcp-variants-workshop-2016/5-Session.pdf" target="_blank">Session-5</a>
    </td>
  </tr>
  
  <tr>
    <td>
      &nbsp;
    </td>
    
    <td>
    </td>
    
    <td>
    </td>
  </tr>
  
  <tr class="odd">
    <td>
      ns-3 overview given to <a href="http://wing.nitk.ac.in/resources/SWN-2014-Brochure.pdf"> NITK ns-3 workshop</a>
    </td>
    
    <td align="center">
      &nbsp;July 2014&nbsp;
    </td>
    
    <td align="center">
      <a href="/docs/ns-3-overview-july-2014.pdf" target="_blank">pdf</a>
    </td>
  </tr>
  
  <tr>
    <td>
      &nbsp;
    </td>
    
    <td>
    </td>
    
    <td>
    </td>
  </tr>
  
  <tr class="even">
    <td>
      Tutorials at the ns-3 Consortium kickoff meeting
    </td>
    
    <td align="center">
      &nbsp;March 2013&nbsp;
    </td>
    
    <td align="center">
      <a href="/consortium/activities/annual-meeting-march-2013/" target="_blank">pdf (scroll down to find links)</a>
    </td>
  </tr>
  
  <tr>
    <td>
      &nbsp;
    </td>
    
    <td>
    </td>
    
    <td>
    </td>
  </tr>
  
  <tr class="odd">
    <td>
      Walid Younes&#8217;s tutorial
    </td>
    
    <td align="center">
      &nbsp;June 2013&nbsp;
    </td>
    
    <td align="center">
      <a href="/tutorials/ns-3-tutorial-Walid-Younes.pdf" target="_blank">pdf</a>
    </td>
  </tr>
  
  <tr>
    <td>
      &nbsp;
    </td>
    
    <td>
    </td>
    
    <td>
    </td>
  </tr>
  
  <tr class="even">
    <td>
      Konstantinos Katsaros&#8217;s tutorials for PGSDP workshop
    </td>
    
    <td align="center">
      &nbsp;2012&nbsp;
    </td>
    
    <td align="center">
      <a href="http://personal.ee.surrey.ac.uk/Personal/K.Katsaros/ns3.html" target="_blank">various pdfs</a>
    </td>
  </tr>
  
  <tr>
    <td>
      &nbsp;
    </td>
    
    <td>
    </td>
    
    <td>
    </td>
  </tr>
  
  <tr class="odd">
    <td>
      ns-3 tutorial at GENI <a href="http://www.geni.net/?p=1878">GEC 9</a>
    </td>
    
    <td align="center">
      &nbsp;November 4, 2010&nbsp;
    </td>
    
    <td align="center">
      <a href="/tutorials/geni-tutorial-part1.pdf">Part 1 (pdf)</a>,<br /> <a href="/tutorials/geni-tutorial-part2.pdf">Part 2 (pdf)</a>,<br /> <a href="/tutorials/geni-dce.pdf">DCE (pdf)</a>,<br /> <a href="/tutorials/geni-nepi.pdf">NEPI (pdf)</a>
    </td>
  </tr>
  
  <tr class="even">
    <td>
      Gustavo Carneiro's lab brief on ns-3
    </td>
    
    <td align="center">
      &nbsp;April 20, 2010&nbsp;
    </td>
    
    <td align="center">
      <a href="/tutorials/NS-3-LABMEETING-1.pdf">pdf</a>,<br /> <a href="/tutorials/NS-3-LABMEETING.odp">odp</a>
    </td>
  </tr>
  
  <tr class="odd">
    <td>
      George Riley's ACM SpringSim keynote on ns-3
    </td>
    
    <td align="center">
      &nbsp;March 2010&nbsp;
    </td>
    
    <td align="center">
      <a href="/tutorials/SpringSim-2010-Riley.pptx">ppt</a>
    </td>
  </tr>
  
  <tr class="even">
    <td>
      Mathieu Lacage's <a href="http://trilogy-project.org/events/summerschool.html">Trilogy summer school</a> talks
    </td>
    
    <td align="center">
      &nbsp;August 27th, 2009&nbsp;
    </td>
    
    <td align="center">
      <a href="http://inl.info.ucl.ac.be/tutorials/tfiss09-lacage">video</a>,<br /> <a href="/tutorials/trilogy-summer-school.pdf">pdf</a>,<br /> <a href="/tutorials/trilogy-summer-school-handouts.pdf">pdf handouts</a><br /> <a href="http://code.nsnam.org/mathieu/tutorial-trilogy">latex/examples source</a>
    </td>
  </tr>
  
  <tr class="odd">
    <td>
      Mathieu Lacage's Tunis tutorial
    </td>
    
    <td align="center">
      &nbsp;April 7, 2009&nbsp;
    </td>
    
    <td align="center">
      <a href="/tutorials/ns-3-tutorial-tunis-apr09.pdf">pdf</a>
    </td>
  </tr>
  
  <tr class="even">
    <td>
      Worshop on ns-3: Tutorial part 1 (ns-3 features)
    </td>
    
    <td align="center">
      &nbsp;March 2, 2009&nbsp;
    </td>
    
    <td align="center">
      <a href="/workshops/wns3-2009/ns-3-tutorial-part-1.pdf">pdf</a>,&nbsp;&nbsp;<a href="/workshops/wns3-2009/wns3-helper.cc">sample helper program</a>
    </td>
  </tr>
  
  <tr class="odd">
    <td>
      Worshop on ns-3: Tutorial part 2 (end-to-end system)
    </td>
    
    <td align="center">
      &nbsp;March 2, 2009&nbsp;
    </td>
    
    <td align="center">
      <a href="/workshops/wns3-2009/ns-3-tutorial-part-2.pdf">pdf</a>,&nbsp;&nbsp;<a href="/workshops/wns3-2009/wns3-lowlevel.cc">sample low-level program</a>
    </td>
  </tr>
  
  <tr class="even">
    <td>
      <a href="http://www.simutools.org/ns3.shtml">Simutools 08</a>
    </td>
    
    <td align="center">
      &nbsp;March 5, 2008&nbsp;
    </td>
    
    <td align="center">
      <a href="/tutorials/simutools08/ns-3-tutorial-slides.pdf">pdf</a>,<br /> <a href="/tutorials/simutools08/ns-3-tutorial-slides.ppt">ppt</a>,<br /> <a href="/tutorials/simutools08/ns-3-tutorial-handouts.pdf">handouts (pdf)</a>,<br /> <a href="/tutorials/simutools08/ns-3-tutorial.tar.gz">source code</a>
    </td>
  </tr>
</table>
