---
title: Authors
layout: page
permalink: /releases/ns-3-34/authors/
---
The following people made source code contributions (commits) during the ns-3.34 development period:

  * Aditya Chirania <adityachirania97@gmail.com>
  * Alberto Gallegos <shattered.feelings@gmail.com>
  * Alessandro Aimi <alleaimi95@gmail.com>
  * Alexander Krotov <ilabdsf@gmail.com>
  * Ameya Deshpande <ameyanrd@outlook.com>
  * Biljana Bojovic <biljana.bojovic@cttc.es>
  * Bhaskar Kataria <bhaskar.k7920@gmail.com>
  * Chappidi Yoga Satwik <satwik.chappidi@iitkgp.ac.in>
  * Chetan Agrawal <chetanag35@gmail.com>
  * Christophe Delahaye <christophe.delahaye@orange.com>
  * Davide Magrin <magrin.davide@gmail.com>
  * Eduardo Almeida <enmsa@outlook.pt>
  * Federico Guerra <federico@guerra-tlc.com>
  * Gabriel Arrobo <gab.arrobo@gmail.com>
  * Gustavo J. A. M. Carneiro <gjc@inescporto.pt>
  * Mattia Lecci <mattia.lecci@gmail.com>
  * Mauriyin <haoyin@uw.edu>
  * Mohit P. Tahiliani <tahiliani.nitk@gmail.com>
  * Mohtashim Nawaz <nawazmohtashim@gmail.com>
  * Muhammad Iqbal CR <kyuucr@gmail.com>
  * Nitya Chandra <nityachandra6@gmail.com>
  * Vivek Jain <rnes, Jr <barnes26@llnl.gov>
  * Rediet (Orange) <getachew.redieteab@orange.com>
  * Sachin Nayak <sachinn@uw.edu>
  * Sébastien Deronne <sebastien.deronne@gmail.com>
  * Sharan Naribole <sharan.naribole@gmail.com>
  * Shiva Gantha <ganthashiva@gmail.com>
  * Shobhit Chaurasia <000shobhitchaurasia@gmail.com>
  * Siddharth Singh <siddharth12375@gmail.com>
  * Stefano Avallone <stavallo@unina.it>
  * Steven Smith <smith84@llnl.gov>
  * Tom Henderson <tomh@tomh.org>
  * Tommaso Pecorella <tommaso.pecorella@unifi.it>
  * Tommaso Zugno <tommasozugno@gmail.com>
  * Vivek Jain <jain.vivek.anand@gmail.com>
  * ZakariaHelalArzoo <arzoozakaria@gmail.com>
  * ZorazeAli <zoraze.ali@cttc.es>
