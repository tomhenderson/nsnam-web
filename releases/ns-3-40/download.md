---
title: Download
layout: page
permalink: /releases/ns-3-40/download/
---
Please click the following link to download ns-3.40, released Sept. 27, 2023:

  * [ns-allinone-3.40](/releases/ns-allinone-3.40.tar.bz2) (compressed source code archive)

The [sha1sum](https://linux.die.net/man/1/sha1sum) of the ns-allinone-3.40.tar.bz2 release archive is 1687ee0579ba8c0f4eb681fc6571e4dfc67d6306
