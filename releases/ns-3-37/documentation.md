---
title: Documentation
layout: page
permalink: /releases/ns-3-37/documentation/
---
  * **Tutorial:** an introduction into downloading, setting up, and using builtin models: [pdf](/docs/release/3.37/tutorial/ns-3-tutorial.pdf), [html (single page)](/docs/release/3.37/tutorial/singlehtml/index.html), [html (split page)](/docs/release/3.37/tutorial/html/index.html)
  * **Manual:** an in-depth coverage of the architecture and core of ns-3: [pdf](/docs/release/3.37/manual/ns-3-manual.pdf), [html (single page)](/docs/release/3.37/manual/singlehtml/index.html), [html (split page)](/docs/release/3.37/manual/html/index.html)
  * **Model Library:** documentation on individual protocol and device models that build on the ns-3 core: [pdf](/docs/release/3.37/models/ns-3-model-library.pdf), [html (single page)](/docs/release/3.37/models/singlehtml/index.html), [html (split page)](/docs/release/3.37/models/html/index.html)
  * **Contributing Guide:** documentation on how to contribute to ns-3: [pdf](/docs/release/3.37/contributing/ns-3-contributing.pdf), [html (single page)](/docs/release/3.37/contributing/singlehtml/index.html), [html (split page)](/docs/release/3.37/contributing/html/index.html)
  * [API Documentation](/docs/release/3.37/doxygen/index.html):&nbsp; Coverage of the C++ APIs using Doxygen.
  * Documentation of the [Bake](https://www.nsnam.org/docs/bake/tutorial/html/index.html) build tool
  * [Release Errata](/wiki/Errata) 
