---
title: Authors
layout: page
permalink: /releases/ns-3-41/authors/
---
The following people made source code and documentation contributions (commits) during the ns-3.41 development period:

* Alberto Gallegos Ramonet <alramonet@is.tokushima-u.ac.jp>
* Alessio Bugetti <alessiobugetti98@gmail.com>
* Alexander Krotov <krotov@iitp.ru>
* Andrea Lacava <thecave003@gmail.com>
* André Apitzsch <andre.apitzsch@etit.tu-chemnitz.de>
* Kavya Bhat <kavyabhat@gmail.com>
* Biljana Bojovic <bbojovic@cttc.es>
* Chaz Maschman
* Doug Blough <doug.blough@ece.gatech.edu>
* Eduardo Almeida <enmsa@outlook.pt>
* Gabriel Ferreira <gabrielcarvfer@gmail.com>
* Heran Yang <heran55@126.com>
* Lars Toenning <dev@ltoenning.de>
* Levente Mészáros <levy@omnetpp.org>
* Martin Quinson <mquinson@debian.org>
* Menglei Zhang <menglei@nyu.edu>
* Sandra Lagen <slagen@cttc.es>
* Sébastien Deronne <sebastien.deronne@gmail.com>
* Stefano Avallone <stavallo@unina.it>
* Szymon Szott <szott@kt.agh.edu.pl>
* Tolik Zinovyev <tolik@bu.edu>
* Tom Henderson <tomh@tomh.org>
* Tommaso Pecorella <tommaso.pecorella@unifi.it>
