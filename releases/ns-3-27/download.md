---
title: Download
layout: page
permalink: /releases/ns-3-27/download/
---
Please click the following link: [ns-allinone-3.27](https://www.nsnam.org/release/ns-allinone-3.27.tar.bz2).

A source code patch to update ns-3.26 release to ns-3.27 release is available [here](https://www.nsnam.org/release/patches/ns-3.26-to-ns-3.27.patch). Other patches to migrate older versions of ns-3 (back to ns-3.17) to the latest version can be found in the same directory; they must be applied sequentially to upgrade across multiple releases.
