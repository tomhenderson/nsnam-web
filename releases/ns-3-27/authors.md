---
title: Authors
layout: page
permalink: /releases/ns-3-27/authors/
---
The following people made code contributions (commits) during the ns-3.27 development period:

  * Akin Soysal <akinsoysal@gmail.com>
  * Alexander Krotov <krotov@iitp.ru>
  * Alina Quereilhac <aquereilhac@gmail.com>
  * Ankit Deepak <adadeepak8@gmail.com>
  * Biljana Bojovic <biljana.bojovic@gmail.com>
  * Christoph Döpmann <doepmanc@informatik.hu-berlin.de>
  * Danilo Abrignani <dabrignani@gmail.com>
  * Federico Guerra <federico@guerra-tlc.com>
  * Francisco Javier Sanchez Roselly <franciscojavier.sanchezroselly@ujaen.es>
  * Getachew Redieteab <redieteab.orange@gmail.com>
  * Izydor Sokoler <Izydor.sokoler@gmail.com>
  * John Abraham <john.abraham.in@gmail.com>
  * Junling Bu <linlinjavaer@gmail.com>
  * Lauri Sormunen <lauri.sormunen@magister.fi>
  * Luciano J Chaves <ljerezchaves@gmail.com>
  * Lynne Salameh <l.salameh@cs.ucl.ac.uk>
  * Marco Miozzo <marco.miozzo@cttc.es>
  * Matias Richart <mrichart@fing.edu.uy>
  * Menglei Zhang <menglei@nyu.edu>
  * Michele Polese <michele.polese@gmail.com>
  * Mingyu Park <darkpmg@naver.com>
  * Miralem Mehic <miralemmehic@gmail.com>
  * Mohit P. Tahiliani <tahiliani@nitk.edu.in>
  * Natale Patriciello <natale.patriciello@gmail.com>
  * Nichit Bodhak Goel <nichit93@gmail.com>
  * Nicola Baldo <nicola@baldo.biz>
  * Pasquale Imputato <p.imputato@gmail.com>
  * Peter D. Barnes, Jr. <barnes26@llnl.gov>
  * Piyush Aggarwal <piyush8311@gmail.com>
  * Robert Ammon <ammo6818@vandals.uidaho.edu>
  * Rohit P. Tahiliani <tahiliar@tcd.ie>
  * Sébastien Deronne <sebastien.deronne@gmail.com>
  * Shravya Ks <shravya.ks0@gmail.com>
  * Sourabh Jain <jainsourabh679@gmail.com>
  * Stefano Avallone <stavallo@unina.it>
  * Tom Henderson <tomh@tomh.org>
  * Tommaso Pecorella <tommaso.pecorella@unifi.it>
  * Toshio Ito <debug.ito@gmail.com>
  * Varun Reddy <varunamarreddy@gmail.com>
  * Vedran Miletić <rivanvx@gmail.com>
  * Zoraze Ali <zoraze.ali@cttc.es>
