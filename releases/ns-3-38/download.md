---
title: Download
layout: page
permalink: /releases/ns-3-38/download/
---
Please click the following link to download ns-3.38, released March 17, 2023:

  * [ns-allinone-3.38](/releases/ns-allinone-3.38.tar.bz2) (compressed source code archive)

The [sha1sum](https://linux.die.net/man/1/sha1sum) of the ns-allinone-3.38.tar.bz2 release archive is b33980d82fe865965ac9ff95cfd9157bff5a91e4
