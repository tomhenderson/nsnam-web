---
title: Authors
layout: page
permalink: /releases/ns-3-38/authors/
---
The following people made source code contributions (commits) during the ns-3.38 development period:

* Alberto Gallegos Ramonet <alramonet@is.tokushima-u.ac.jp>
* André Apitzsch <andre.apitzsch@etit.tu-chemnitz.de>
* Biljana Bojovic <biljana.bojovic@cttc.es>
* Bill Tao <toyjet8@gmail.com>
* Chandrakant Jena <chandrakant.barcelona@gmail.com>
* Eduardo Almeida <enmsa@outlook.pt>
* Gabriel Arrobo <gab.arrobo@gmail.com>
* Gabriel Ferreira <gabrielcarvfer@gmail.com>
* Lars Toenning <dev@ltoenning.de>
* Matteo Drago <matteo.drago1994@gmail.com>
* Matteo Pagin <mattpagg@gmail.com>
* Peter D. Barnes, Jr <barnes26@llnl.gov>
* Sébastien Deronne <sebastien.deronne@gmail.com>
* Sharan Naribole <sharan.naribole@gmail.com>
* Stefano Avallone <stavallo@unina.it>
* Tom Henderson <tomh@tomh.org>
* Tommaso Pecorella <tommaso.pecorella@unifi.it>
* Wouter Overmeire <lodagro@gmail.com>
