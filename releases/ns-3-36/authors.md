---
title: Authors
layout: page
permalink: /releases/ns-3-36/authors/
---
The following people made source code contributions (commits) during the ns-3.36 development period:

* Akash Mondal
* Alexander Krotov
* Ameya Deshpande
* André Apitzsch
* Bhaskar Kataria
* Biljana Bojovic
* Chandrakant jena
* Davide Magrin
* Eduardo Almeida
* Gabriel Arrobo
* Gabriel Ferreira
* Gard Spreemann
* Greg Steinbrecher
* Hossam Khader
* Mohit P. Tahiliani
* Pasquale Imputato
* Peter Barnes
* Philipp Raich
* Pierre Wendling
* Robin Lee
* Ryan Mast
* Sachin Nayak
* Sandra Lagen
* Sébastien Deronne
* Sharan Naribole
* Stefano Avallone
* Szymon Szott
* Tolik Zinovyev
* Tom Henderson
* Tommaso Pecorella
