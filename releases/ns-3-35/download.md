---
title: Download
layout: page
permalink: /releases/ns-3-35/download/
---
Please click the following link to download ns-3.35, released October 1, 2021:

  * [ns-allinone-3.35](/releases/ns-allinone-3.35.tar.bz2) (compressed source code archive)

For users of the ns-3.34 release, a patch is available to update code to ns-3.35, at [this link](/release/patches/ns-3.34-to-ns-3.35.patch).  Other patches to migrate older versions of ns-3 (back to ns-3.17) to subsequent release versions can be found in the same directory; they must be applied sequentially to upgrade across multiple releases.

The [sha1sum](https://linux.die.net/man/1/sha1sum) of the ns-allinone-3.35.tar.bz2 release archive is 943a19c6b92b36d923671ae90a065f7ffb0dbabc
