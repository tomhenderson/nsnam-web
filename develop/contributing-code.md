---
title: Contributing Code
layout: page
permalink: /develop/contributing-code/
---
A [guide on contributing to ns-3](https://www.nsnam.org/docs/contributing/html/index.html) is now maintained as part of the project documentation.  If you have further questions after reading this document, please contact one of the maintainers or ask on the mailing list or on Zulip chat.
