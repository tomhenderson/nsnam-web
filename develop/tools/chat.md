---
title: Chat
layout: page
permalink: /develop/tools/chat/
---
ns-3 discussions take place from time to time in a [Zulip](https://ns-3.zulipchat.com) chatroom.

In the past, ns-3 used the #ns-3 IRC channel hosted on the [irc.freenode.net](http://freenode.net) network and the [freenode web interface](http://webchat.freenode.net?channels=ns-3).
