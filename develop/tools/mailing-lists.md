---
title: Mailing lists
layout: page
permalink: /develop/tools/mailing-lists/
---

People interested in ns-3 development should subscribe to and read the ns-developers mailing list, a Google Group since November 2022: [Google Groups page](https://groups.google.com/g/ns-developers)&nbsp;&nbsp;[Archives prior to November 2022](http://mailman.isi.edu/pipermail/ns-developers/) 

For user-oriented mail (getting help with using ns-3) there is also the <a name="ns-3-users"></a>**ns-3-users** mailing list: [Google Groups page](https://groups.google.com/forum/?fromgroups#!forum/ns-3-users)

A few additional developer-oriented lists predate our use of GitLab notifications, but the archives may still be useful:
* ns-3-reviews: ns-3 code reviews. [Archives](http://groups.google.com/group/ns-3-reviews) 
* ns-commits: every Mercurial commit pushed to one of the repositories hosted on code.nsnam.org generates an email describing the commit, and nightly validation messages. [Archives](http://mailman.isi.edu/pipermail/ns-commits/) 
* ns-bugs: Automatic mail from Bugzilla database updates.  [Archives](http://mailman.isi.edu/pipermail/ns-bugs/)

